Example of Java SE project (jar execuatble) using CMake with comfortable configure shell script and Gradle build tools.

Contributor Valerii Kanunik <valerii-65@yandex.ua>

To build and launch the application run:
mkdir build; cd build; cmake ..; make
java -jar jar-ins.jar

To manually build and run:
javac java/instance/Main.java
java -cp java/ instance.Main
